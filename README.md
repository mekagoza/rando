# Rando

Command line application to generate random/fake information. I started this to help me generate usernames.

## Help
```shell
me@host ~> fake help
fake_cli 1.0.0-alpha
Mekagoza <mekagoza@fastmail.com>
CLI app to generate fake/random data

USAGE:
    fake <SUBCOMMAND>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

SUBCOMMANDS:
    address    Generates fake address-related data
    bool       Generates a random boolean
    company    Generates corporate nonsense
    help       Prints this message or the help of the given subcommand(s)
    lorem      Generates fake lorem ipsum type stuff
    name       Generates random names
    net        Generates fake internet-related data
    num        Generates random numbers
    phone      Generates fake phone numbers

```
## Example Session
```shell
me@host ~> fake net help
fake-net 
Generates fake internet-related data

USAGE:
    fake net <SUBCOMMAND>

FLAGS:
    -h, --help    Prints help information

SUBCOMMANDS:
    color         Generates a hex color like '#FFFFFF'
    email         Generates a possibly real email address at a big provider
    help          Prints this message or the help of the given subcommand(s)
    ip            Generates an IPv4 address
    ip6           Generates an IPv6
    pass          Generates a password
    provider      One of "gmail.com", "yahoo.com", "hotmail.com"
    safe-email    Generates an 'example.com' email
    tld           Generates an old-fashioned TLD
    ua            Generates a user-agent
    username      Generates a username
me@host ~> fake net ip6
52E5:5531:591E:69ED:6171:B25:C4B7:5AC0
me@host ~> fake net username
mckayla_repellendus
me@host ~> fake net pass
tY69eUbPG
me@host ~> fake net help pass
fake-net-pass 
Generates a password

USAGE:
    fake net pass [ARGS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

ARGS:
    <min-length>    Minimum length [default: 8]
    <max-length>    Maximum length [default: 24]
me@host ~> fake net pass 16 16
qBItYfOrDc0gPvSj
me@host ~> fake company
fake-company 
Generates corporate nonsense

USAGE:
    fake company <SUBCOMMAND>

FLAGS:
    -h, --help    Prints help information

SUBCOMMANDS:
    bs            Generates corporate BS
    buzzword      Generates a buzzword
    help          Prints this message or the help of the given subcommand(s)
    industry      Generates an industry
    name          Generates a company name
    phrase        Generates a catchphrase
    profession    Generates a corporate profession
    suffix        Generates a corporate suffix like 'LLC'
me@host ~ [1]> fake company bs
deploy next-generation users
me@host ~> fake company bs
incubate scalable infomediaries
me@host ~> fake company phrase
Multi-lateral system-worthy collaboration
```
