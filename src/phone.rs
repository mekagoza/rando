use clap::ArgMatches;

use error::{Error, Result};

pub fn exec(matches: &ArgMatches) -> Result<String> {
    use fake::faker::*;

    match matches.subcommand() {
        ("number", _) => Ok(Faker::phone_number()),
        ("cell", _) => Ok(Faker::cell_number()),
        ("with-format", Some(args)) => {
            let format = get!(args, "format", String);
            Ok(Faker::phone_number_with_format(format))
        }
        (_, _) => Err(Error::missing(matches.usage())),
    }
}
