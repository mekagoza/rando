use clap::ArgMatches;

use error::{Error, Result};

use join;

pub fn exec(matches: &ArgMatches) -> Result<String> {
    use fake::faker::*;

    match matches.subcommand() {
        ("word", _) => Ok(Faker::word().into()),
        ("words", Some(args)) => {
            let count = get!(args, "count", usize);
            Ok(join(Faker::words(count), " "))
        }
        ("sentence", Some(args)) => {
            let count = get!(args, "count", usize);
            Ok(Faker::sentence(count, count / 10))
        }
        ("sentences", Some(args)) => {
            let count = get!(args, "count", usize);
            Ok(join(Faker::sentences(count), " "))
        }
        ("paragraph", Some(args)) => {
            let count = get!(args, "count", usize);
            Ok(Faker::paragraph(count, count / 10))
        }
        ("paragraphs", Some(args)) => {
            let count = get!(args, "count", usize);
            Ok(join(Faker::paragraphs(count), " "))
        }
        (_, _) => Err(Error::missing(matches.usage())),
    }
}
