use clap::ArgMatches;

use error::{Error, Result};

pub fn exec(matches: &ArgMatches) -> Result<String> {
    use fake::faker::*;

    if matches.subcommand_name().is_none() {
        return Ok(<Faker as Name>::name());
    }

    match matches.subcommand() {
        ("first", _) => Ok(Faker::first_name().into()),
        ("last", _) => Ok(Faker::last_name().into()),
        ("prefix", _) => Ok(Faker::prefix().into()),
        ("suffix", _) => Ok(<Faker as Name>::suffix().into()),
        ("with-middle", _) => Ok(Faker::name_with_middle()),
        ("title-desc", _) => Ok(Faker::title_descriptor().into()),
        ("title-level", _) => Ok(Faker::title_level().into()),
        ("title-job", _) => Ok(Faker::title_job().into()),
        ("title", _) => Ok(Faker::title()),
        (_, _) => Err(Error::missing(matches.usage())),
    }
}
