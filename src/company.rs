use clap::ArgMatches;

use error::{Error, Result};

pub fn exec(matches: &ArgMatches) -> Result<String> {
    use fake::faker::*;

    match matches.subcommand() {
        ("name", _) => Ok(<Faker as Company>::name()),
        ("buzzword", _) => Ok(Faker::buzzword().into()),
        ("suffix", _) => Ok(<Faker as Company>::suffix().into()),
        ("phrase", _) => Ok(Faker::catch_phase()),
        ("bs", _) => Ok(Faker::bs()),
        ("profession", _) => Ok(Faker::profession().into()),
        ("industry", _) => Ok(Faker::industry().into()),
        (_, _) => Err(Error::missing(matches.usage())),
    }
}
