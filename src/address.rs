use clap::ArgMatches;

use error::{Error, Result};

pub fn exec(matches: &ArgMatches) -> Result<String> {
    use fake::faker::*;

    match matches.subcommand() {
        ("tz", _) => Ok(Faker::time_zone().into()),
        ("city-suffix", _) => Ok(Faker::city_suffix().into()),
        ("city-prefix", _) => Ok(Faker::city_prefix().into()),
        ("street-suffix", _) => Ok(Faker::street_suffix().into()),
        ("state", _) => Ok(Faker::state().into()),
        ("state-abbr", _) => Ok(Faker::state_abbr().into()),
        ("city", _) => Ok(Faker::city()),
        ("street", _) => Ok(Faker::street_name()),
        ("bnum", _) => Ok(Faker::building_number()),
        ("paddr", _) => Ok(Faker::street_address()),
        ("saddr", _) => Ok(Faker::secondary_address()),
        ("zip", _) => Ok(Faker::postcode()),
        ("lat", _) => Ok(Faker::latitude()),
        ("long", _) => Ok(Faker::longitude()),
        (_, _) => Err(Error::missing(matches.usage())),
    }
}
