use clap::ArgMatches;

use error::{Error, Result};

pub fn exec(matches: &ArgMatches) -> Result<String> {
    use fake::faker::*;

    match matches.subcommand() {
        ("provider", _) => Ok(Faker::free_email_provider().into()),
        ("tld", _) => Ok(Faker::domain_suffix().into()),
        ("username", _) => Ok(Faker::user_name()),
        ("email", _) => Ok(Faker::free_email()),
        ("safe-email", _) => Ok(Faker::safe_email()),
        ("pass", Some(args)) => {
            let raw_min = get!(args, "min-length", String);
            let raw_max = get!(args, "max-length", String);

            let (min, max): (usize, usize) = (raw_min.parse()?, raw_max.parse()?);

            Ok(Faker::password(min, max))
        }
        ("ip", _) => Ok(Faker::ipv4()),
        ("ip6", _) => Ok(Faker::ipv6()),
        ("color", _) => Ok(Faker::color()),
        ("ua", _) => Ok(Faker::user_agent().into()),
        (_, _) => Err(Error::missing(matches.usage())),
    }
}
