use std::result::Result as Res;

use clap::ArgMatches;

use error::{Error, Result};

pub fn exec(matches: &ArgMatches) -> Result<String> {
    use fake::faker::*;

    match matches.subcommand() {
        ("digit", _) => Ok(Faker::digit()),
        ("number", Some(args)) => {
            let count = get!(args, "count", usize);
            Ok(Faker::number(count))
        }
        ("between", Some(args)) => {
            let raw_start = get!(args, "start", String);
            let raw_end = get!(args, "end", String);

            let (start, end): (Res<usize, _>, Res<usize, _>) = (raw_start.parse(), raw_end.parse());

            match (start, end) {
                (Ok(s), Ok(e)) => Ok(Faker::between(s, e).to_string()),
                _ => {
                    let (start, end): (f64, f64) = (raw_start.parse()?, raw_end.parse()?);
                    Ok(Faker::between(start, end).to_string())
                }
            }
        }
        (_, _) => Err(Error::missing(matches.usage())),
    }
}
