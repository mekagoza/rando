use clap::{App, AppSettings, Arg, SubCommand};

pub fn build<'a, 'b>() -> App<'a, 'b> {
    App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!(", "))
        .about(crate_description!())
        .subcommand(lorem())
        .subcommand(phone())
        .subcommand(num())
        .subcommand(name())
        .subcommand(company())
        .subcommand(net())
        .subcommand(address())
        .subcommand(SubCommand::with_name("bool").about("Generates a random boolean"))
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .setting(AppSettings::VersionlessSubcommands)
}

fn lorem<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name("lorem")
        .about("Generates fake lorem ipsum type stuff")
        .subcommand(SubCommand::with_name("word").about("Generates a word"))
        .subcommand(
            SubCommand::with_name("words").about("Generates words").arg(
                Arg::with_name("count")
                    .help("Number of words to generate")
                    .required(true)
                    .index(1),
            ),
        )
        .subcommand(
            SubCommand::with_name("sentence")
                .about("Generates a sentence")
                .arg(
                    Arg::with_name("count")
                        .help("Rough number of words in sentence")
                        .required(true)
                        .index(1),
                ),
        )
        .subcommand(
            SubCommand::with_name("sentences")
                .about("Generates sentences")
                .arg(
                    Arg::with_name("count")
                        .help("Number of sentences to generate")
                        .required(true)
                        .index(1),
                ),
        )
        .subcommand(
            SubCommand::with_name("paragraph")
                .about("Generates a paragraph")
                .arg(
                    Arg::with_name("count")
                        .help("Number of sentences in paragraph")
                        .required(true)
                        .index(1),
                ),
        )
        .subcommand(
            SubCommand::with_name("paragraphs")
                .about("generates paragraphs")
                .arg(
                    Arg::with_name("count")
                        .help("Number of paragraphs")
                        .required(true)
                        .index(1),
                ),
        )
        .setting(AppSettings::SubcommandRequiredElseHelp)
}

fn net<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name("net")
        .about("Generates fake internet-related data")
        .subcommand(
            SubCommand::with_name("provider")
                .about(r#"One of "gmail.com", "yahoo.com", "hotmail.com""#),
        )
        .subcommand(SubCommand::with_name("tld").about("Generates an old-fashioned TLD"))
        .subcommand(SubCommand::with_name("username").about("Generates a username"))
        .subcommand(
            SubCommand::with_name("email")
                .about("Generates a possibly real email address at a big provider"),
        )
        .subcommand(SubCommand::with_name("safe-email").about("Generates an 'example.com' email"))
        .subcommand(
            SubCommand::with_name("pass")
                .about("Generates a password")
                .arg(
                    Arg::with_name("min-length")
                        .help("Minimum length")
                        .default_value("8")
                        .index(1),
                )
                .arg(
                    Arg::with_name("max-length")
                        .help("End of the range (exclusive)")
                        .default_value("24")
                        .index(2),
                ),
        )
        .subcommand(SubCommand::with_name("ip").about("Generates an IPv4 address"))
        .subcommand(SubCommand::with_name("ip6").about("Generates an IPv6"))
        .subcommand(SubCommand::with_name("color").about("Generates a hex color like '#FFFFFF"))
        .subcommand(SubCommand::with_name("ua").about("Generates a user-agent"))
        .setting(AppSettings::SubcommandRequiredElseHelp)
}

fn phone<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name("phone")
        .about("Generates fake phone numbers")
        .subcommand(SubCommand::with_name("number").about("Generates a phone number"))
        .subcommand(SubCommand::with_name("cell").about("Generates a cell phone number"))
        .subcommand(
            SubCommand::with_name("with-format")
                .about("Generates phone numbers according to a format string")
                .arg({
                    let formats = r#"Examples of valid formats: 
    "(N##) ###-#### x###",
    "(N##) ###-#### x####",
    "(N##) ###-#### x#####",
    "(N##) ###-####",
    "1-N##-###-#### x###",
    "1-N##-###-#### x####",
    "1-N##-###-#### x#####",
    "1-N##-###-####",
    "N##-###-#### x###",
    "N##-###-#### x####",
    "N##-###-#### x#####",
    "N##-###-####",
    "N##.###.#### x###",
    "N##.###.#### x####",
    "N##.###.#### x#####",
    "N##.###.####""#;
                    Arg::with_name("format")
                        .help("the format")
                        .long_help(formats)
                        .required(true)
                        .index(1)
                }),
        )
        .setting(AppSettings::SubcommandRequired)
}

fn address<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name("address")
        .about("Generates fake address-related data")
        .subcommand(SubCommand::with_name("tz").about("Generates a time zone"))
        .subcommand(
            SubCommand::with_name("city-suffix").about("Generates a city suffix like 'ville'"),
        )
        .subcommand(
            SubCommand::with_name("city-prefix").about("Generates a city prefix like 'New'"),
        )
        .subcommand(
            SubCommand::with_name("street-suffix").about("Generates a street suffix like 'Trail'"),
        )
        .subcommand(SubCommand::with_name("state").about("Generates a US state"))
        .subcommand(
            SubCommand::with_name("state-abbr").about("Generates a US state 2-letter abbreviation"),
        )
        .subcommand(SubCommand::with_name("city").about("Generates a city"))
        .subcommand(SubCommand::with_name("street").about("Generates a street name"))
        .subcommand(SubCommand::with_name("bnum").about("Generates a building number"))
        .subcommand(SubCommand::with_name("paddr").about("Generates a primary street address"))
        .subcommand(SubCommand::with_name("saddr").about("Generates a secondary address line"))
        .subcommand(SubCommand::with_name("zip").about("Generates a zip code"))
        .subcommand(SubCommand::with_name("lat").about("Generates a latitude"))
        .subcommand(SubCommand::with_name("long").about("Generates a longitude"))
        .setting(AppSettings::SubcommandRequiredElseHelp)
}

fn company<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name("company")
        .about("Generates corporate nonsense")
        .subcommand(SubCommand::with_name("name").about("Generates a company name"))
        .subcommand(SubCommand::with_name("buzzword").about("Generates a buzzword"))
        .subcommand(
            SubCommand::with_name("suffix").about("Generates a corporate suffix like 'LLC'"),
        )
        .subcommand(SubCommand::with_name("phrase").about("Generates a catchphrase"))
        .subcommand(SubCommand::with_name("bs").about("Generates corporate BS"))
        .subcommand(SubCommand::with_name("profession").about("Generates a corporate profession"))
        .subcommand(SubCommand::with_name("industry").about("Generates an industry"))
        .setting(AppSettings::SubcommandRequiredElseHelp)
}

fn name<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name("name")
        .about("Generates random names")
        .subcommand(SubCommand::with_name("first").about("Generates a first name"))
        .subcommand(SubCommand::with_name("last").about("Generates a last name"))
        .subcommand(SubCommand::with_name("prefix").about("Generates a prefix like 'Ms.'"))
        .subcommand(SubCommand::with_name("suffix").about("Generates a suffix like 'Sr.'"))
        .subcommand(
            SubCommand::with_name("with-middle").about("Generates a name including a middle name"),
        )
        .subcommand(
            SubCommand::with_name("title-desc")
                .about("Generates a title descriptor like 'Senior' or 'Global'"),
        )
        .subcommand(
            SubCommand::with_name("title-level")
                .about("Generates a title level like 'Security' or 'Group'"),
        )
        .subcommand(
            SubCommand::with_name("title-job").about("Generates a job description like 'Engineer'"),
        )
        .subcommand(SubCommand::with_name("title").about("Generates a full job title"))
        .help_short("By default generates a first and last name")
}

fn num<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name("num")
        .about("Generates random numbers")
        .subcommand(SubCommand::with_name("digit").about("Generates a digit"))
        .subcommand(
            SubCommand::with_name("number")
                .about("Generates a number with 1 or more digits")
                .arg(
                    Arg::with_name("count")
                        .help("Number of digits to generate")
                        .required(false)
                        .default_value("2")
                        .index(1),
                ),
        )
        .subcommand(
            SubCommand::with_name("between")
                .about("Generates a number sampled uniformly from a range")
                .arg(
                    Arg::with_name("start")
                        .help("Start of the range (inclusive)")
                        .required(true)
                        .index(1),
                )
                .arg(
                    Arg::with_name("end")
                        .help("End of the range (exclusive)")
                        .required(true)
                        .index(2),
                ),
        )
        .setting(AppSettings::SubcommandRequiredElseHelp)
}
