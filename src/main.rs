#![allow(deprecated)]
#[macro_use]
extern crate clap;
extern crate failure;
extern crate fake;

#[macro_export]
macro_rules! get {
    ($m:ident, $v:expr, $t:ty) => {
        value_t!($m, $v, $t).unwrap_or_else(|e| e.exit())
    };
    ($m:ident.value_of($v:expr), $t:ty) => {
        value_t!($m, $v, $t).unwrap_or_else(|e| e.exit())
    };
}

mod address;
mod cli;
mod company;
mod error;
mod internet;
mod lorem;
mod name;
mod num;
mod phone;

use clap::ArgMatches;

use error::Result;

fn join<T, S>(strings: T, sep: &str) -> String
where
    T: IntoIterator<Item = S>,
    S: AsRef<str>,
{
    strings.into_iter().fold(String::new(), |mut left, right| {
        if !left.is_empty() {
            left.push_str(sep);
            left.push_str(right.as_ref());
            left
        } else {
            right.as_ref().into()
        }
    })
}

mod bool_ {
    use error::Result;

    pub fn exec() -> Result<String> {
        use fake::faker::*;

        Ok(Faker::boolean().to_string())
    }
}

fn run(matches: &ArgMatches) -> Result<String> {
    match matches.subcommand() {
        ("bool", _) => bool_::exec(),
        ("lorem", Some(matches)) => lorem::exec(matches),
        ("phone", Some(matches)) => phone::exec(matches),
        ("num", Some(matches)) => num::exec(matches),
        ("name", Some(matches)) => name::exec(matches),
        ("company", Some(matches)) => company::exec(matches),
        ("net", Some(matches)) => internet::exec(matches),
        ("address", Some(matches)) => address::exec(matches),
        _ => Err(error::missing(matches.usage())),
    }
}

fn main() {
    let matches = cli::build().get_matches();

    let result = run(&matches);

    match result {
        Ok(s) => println!("{}", s),
        Err(e) => eprintln!("Error: {}", e),
    }
}
