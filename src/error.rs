use std::convert::From;
use std::fmt;
use std::num::{ParseFloatError, ParseIntError};

use failure::Fail;

pub type Result<T> = ::std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    // #[fail(display = "Error parsing integer: {}", _0)]
    Int(ParseIntError),
    // #[fail(display = "Error parsing float: {}", _0)]
    Float(ParseFloatError),
    // #[fail(display = "No valid subcommand provided\n {}", _0)]
    MissingCmd(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::Int(e) => write!(f, "Error parsing integer: {}", e),
            Error::Float(e) => write!(f, "Error parsing float: {}", e),
            Error::MissingCmd(e) => write!(f, "No valid subcommand provided: \n{}", e),
        }
    }
}

impl Fail for Error {
    fn cause(&self) -> Option<&Fail> {
        match self {
            Error::Float(e) => Some(e),
            Error::Int(e) => Some(e),
            _ => None,
        }
    }
}

impl Error {
    pub fn missing<S: Into<String>>(name: S) -> Error {
        Error::MissingCmd(name.into())
    }
}

#[allow(dead_code)]
pub fn missing<S: Into<String>>(name: S) -> Error {
    Error::missing(name)
}

impl From<ParseFloatError> for Error {
    fn from(err: ParseFloatError) -> Error {
        Error::Float(err)
    }
}

impl From<ParseIntError> for Error {
    fn from(err: ParseIntError) -> Error {
        Error::Int(err)
    }
}
